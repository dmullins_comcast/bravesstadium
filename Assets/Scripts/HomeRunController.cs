﻿using UnityEngine;
using System.Collections;

public class HomeRunController : MonoBehaviour {

    public GameObject bat;
    public GameObject ball;

    public Vector3 ballEndPos;
    public Vector3 batEndRotation;

    public bool IsEventActive;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("up") && IsEventActive == false)
        {
            IsEventActive = true;
            HomeRun();
        }
    }

    IEnumerator HomeRun()
    {
        iTween.MoveTo(ball, ballEndPos, 1f);
        yield return new WaitForSeconds(1f);
        iTween.RotateTo(bat, batEndRotation, 1f);
        yield return new WaitForSeconds(1f);

    }
}
