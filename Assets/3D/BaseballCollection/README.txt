Thanks for purchasing assets from Tronitec Game Studios.
 

Notes:
------------------------------------------------------------------------------------
Use the items in the 'Files For Max' folder when importing into 3ds Max. Max requires '.3ds' files to have all objects, materials, and texture names less than 9 characters. You will still have to set opacity maps for materials containing transparent textures in (.png), bump maps for objects that use a bump map, and so on. Only the textures needed to open the '.3ds' file in Max have been included in the 'Files For Max' folder. The rest of the textures can be found in the 'Textures' folder.

Please disregard the notes above if the downloaded '.zip' file does not contain a 'Files For Max' folder. Just use the models from the 'Models' folder.


Also, the objects have been divided into 3 seperate folders, 'Materials', 'Models', and 'Textures' for organizational purposes. Before importing your models, it is a good idea to have all models, textures, and materials in the same folder. This will allow your 3d software to locate all files needed for the model/s.